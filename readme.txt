CODE FORKED FROM ORIGINAL CYZ_RGB BLINKM CLONE FIRMWARE PROJECT AT
  http://code.google.com/p/codalyze/wiki/CyzRgb
DUE TO NO RESPONSE FROM ORIGINAL AUTHORS TO SUPPLIED PATCHES.  


Modified by David Court (Fanjita) to add various minor additional functionality,
including:
- Easy support for common-anode or common-cathode RGB LEDs
- Fixed support for persistent storage of uploaded scripts
- Additional script commands:
   - Deep sleep
   - Random delay
   - Jump directly to HSB value
   - Loop N times
   - Relative jump
- Simple makefile targets to burn correct fuse values etc. via avrdude.

I've added functionality that I found useful, and make these patches available to 
anyone else that may find them useful.  I don't plan to actively maintain this fork, but pull
requests are welcome from anyone that would like to submit patches.


ORIGINAL README.TXT FOLLOWS:
----------------------------------------


You may build cyz_rgb for either BlinkM (attiny45) or BlinkM MaxM (attiny44) hardware.
The default is to build for attiny45, but you may specify the MCU environment variable
to build for attiny44. The output hex files will be appended with the name of the MCU,
for example "slave-attiny45.hex" or "master-attiny44.hex"

Use the following command to build for BlinkM MaxM (attiny44):
MCU=attiny44 make

master-attiny45.hex: every about 3 seconds chaneges color and broadcasts a fadeToColor command
slave-attiny45.hex: this will listen for incoming fadeToRGB and goToRGB commands and behave accordingly

I'm currently flashing blinkms using an atmel AVRISP programmer with avrdude and this command line:
avrdude -pt45 -cavrispmkII -Pusb -b115200 -Uflash:w:make/out/master-attiny45.hex:a 

The project is being developed with eclipse 3.4, avr plugin and avr-gcc on mac. Anyway, the makefile 
works well also from the console.
